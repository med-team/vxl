Source: vxl
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: cmake,
               debhelper (>= 9),
               freeglut3-dev,
               libavcodec-dev (>= 6:10~),
               libavfilter-dev,
               libavformat-dev (>= 6:10~),
               libavifile-0.7-dev,
               libbz2-dev,
               libcoin80-dev,
               libdc1394-22-dev [!kfreebsd-i386 !kfreebsd-amd64 !hurd-i386],
               libdcmtk-dev,
               libexpat-dev,
               libgeotiff-dev,
               libtiff-dev,
               libglu-dev,
               libjpeg-dev,
               libmpeg2-4-dev,
               libpng-dev,
               libqt4-dev,
               libqt4-opengl-dev,
               libshp-dev,
               libswscale-dev (>= 6:10~),
               libxerces-c-dev,
               ocl-icd-opencl-dev,
               opencl-headers,
               python-dev,
               zlib1g-dev
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/med-team/vxl
Vcs-Git: https://salsa.debian.org/med-team/vxl.git
Homepage: http://vxl.sf.net

Package: libvxl1.17v5
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Conflicts: libvxl1.17
Replaces: libvxl1.17
Pre-Depends: ${misc:Pre-Depends}
Description: C++ Libraries for Computer Vision Research
 VXL (the Vision-something-Libraries) is a collection of C++ libraries
 designed for computer vision research and implementation. It was created
 from TargetJr and the IUE with the aim of making a light, fast and
 consistent system. VXL is written in ANSI/ISO C++ and is designed to be
 portable over many platforms.

Package: libvxl1-dev
Architecture: any
Section: libdevel
Depends: libvxl1.17v5 (= ${binary:Version}),
         ${misc:Depends}
Provides: libvxl-dev
Description: C++ Libraries for Computer Vision Research (development files)
 VXL (the Vision-something-Libraries) is a collection of C++ libraries
 designed for computer vision research and implementation. It was created
 from TargetJr and the IUE with the aim of making a light, fast and
 consistent system. VXL is written in ANSI/ISO C++ and is designed to be
 portable over many platforms.
 .
 This package contains the development files needed to build your own
 VXL applications.
